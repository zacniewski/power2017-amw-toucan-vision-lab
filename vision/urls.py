from django.conf.urls import url, include
from . import views

urlpatterns = [url(r'^detect-faces/$', views.detect_faces, name='detect_faces'),
               url(r'^submit-image-with-faces/$', views.submit_image_with_faces, name='submit_image_with_faces'),
               url(r'^thresholding/$', views.thresholding, name='thresholding'),
               url(r'^submit-image-to-threshold/$', views.submit_image_with_threshold,
                   name='submit_image_with_threshold'),
               url(r'^keypoints/$', views.keypoints, name='keypoints'),
               url(r'^submit-image-to-keypoints/$', views.submit_image_to_keypoints,
                   name='submit_image_to_keypoints'),
               url(r'^segmentation/$', views.segmentation, name='segmentation'),
               url(r'^submit-image-to-segmentation/$', views.submit_image_to_segmentation,
                   name='submit_image_to_segmentation'),
               url(r'^counting-coins/$', views.coins, name='coins'),
               url(r'^submit-image-to-coins/$', views.submit_image_to_coins,
                   name='submit_image_to_coins'),
               url(r'^detect-face-with-camera/$', views.detect_face_with_camera, name='detect_face_with_camera'),
               url(r'^detect-face-with-camera/clown$', views.detect_face_with_camera_clown, name='detect_face_with_camera_clown'),
               url(r'^detect-face-with-camera/anonymous', views.detect_face_with_camera_anonymous,
                   name='detect_face_with_camera_anonymous'),
               url(r'^detect-face-with-camera/bear$', views.detect_face_with_camera_bear,
                   name='detect_face_with_camera_bear'),
               url(r'^detect-face-with-camera/politician$', views.detect_face_with_camera_politician,
                   name='detect_face_with_camera_politician'),
               url(r'^detect-color-with-camera/$', views.detect_color_with_camera, name='detect_color_with_camera'),
               url(r'^submit-canny/$', views.submit_canny, name='submit_canny'),
               url(r'^canny-detector/(?P<thr1>[0-9]+)/(?P<thr2>[0-9]+)/$', views.canny, name='canny'),
               url(r'^computer-vision-algorithms', views.computer_vision_algorithms, name='computer_vision_algorithms'),
               url(r'^about/$', views.about, name='about'),
               url(r'^authors/$', views.authors, name='authors'),

               url(r'^$', views.vision_lab, name='vision_lab')
               ]