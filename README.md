# README #

System przetwarzania obrazów w przeglądarce

### Cel projektu ###

* Realizacja laboratorium przetwarzania obrazów w przeglądarce internetowej

### Technologie ###

* Python, Django, OpenCV

### Autorzy ###

* Front-end - Paweł Dobrowolski
* Back-end - Artur Zacniewski

### Link ###

* https://www.zacniewski.pl/vision/
